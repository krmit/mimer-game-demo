   function Game() {
	   this.speed=5;
       this.points=0;
       this.crashes =[];
       const game_objects=[];
	   
	   this.add = function(gameObject) {
		   game_objects.push(gameObject);
	   }
       
       this.addCrash = function(gameObject) {
		   this.crashes.push(gameObject);
	   }
	   
	   this.update = function() {
		   for( let i = game_objects.length-1; i>=0; i--) {
              game_objects[i].update();
           }
	   }
   }
   
   const game = new Game();
  
   function CheckerBoard(numberOfBox) {
       game.add(this);
       let board=[];
       let margin={};
     
       for(let i=0; i < numberOfBox; i++) {
           board.push([]);
           for(let j=0; j < numberOfBox; j++) {
               board[i][j]=0; 
           }
       }
       
       let box_side;
       if(totalWidth<totalHeight) {
         box_side=Math.floor(totalWidth*0.9/numberOfBox);
       }else {
         box_side=Math.floor(totalHeight*0.9/numberOfBox);
       }
     
       margin.x=box_side*0.3;
       margin.y=box_side*0.3;
       
	   this.draw = function() {
         for(let i =0; i < numberOfBox+1; i++) {
         line(margin.x+i*box_side,margin.y,
              margin.x+i*box_side, margin.y+box_side*numberOfBox);
      
         line(margin.x, margin.y+i*box_side,
              margin.x+box_side*numberOfBox, margin.y+i*box_side);
         }
         };
    
      this.update = function() {
        this.draw();
      }
      
      this.giveBox = function(pointOnBoard) {
        row = Math.floor((pointOnBoard.x-margin.x)/box_side);
        column = Math.floor((pointOnBoard.y-margin.y)/box_side);
        return {"row":row, "column":column};
      }
      
      this.addMark = function(mark) {
        
        let x=mark.row*box_side+margin.x+box_side*0.5;
        let y=mark.column*box_side+margin.y+box_side*0.5;
        switch(mark.symbole){
            case "x":
                board[mark.row][mark.column]=new markO(x,y,box_side/4);
            break;
            case "o":
                board[mark.row][mark.column]=new markX(x,y,box_side/4);
            break;
            default:
                board[mark.row][mark.column]=new markO(x,y,box_side/4);
} 
        
        
      }
}
  
  const game_board = new CheckerBoard(9);
  
  function Player() {
	        this.draw = function() {
         circle(this.x, this.y,this.size, "black");
 
    };

  };
  
  function markX(x,y, size) {
      game.add(this);
      this.x=x;
      this.y=y;
      this.size=size
      this.draw = function() {
         circle(this.x, this.y,this.size, "black");
    };
      
      this.update = function() {
        this.draw();
      }
      
  };
  
    function markO(x,y,size) {
    game.add(this);
      this.x=x;
      this.y=y;
      this.size=size;
      this.draw = function() {
         circle(this.x, this.y,this.size, "black");
    };
      
      this.update = function() {
        this.draw();
      }
      
  };
  
   const player = new Player();
  
    function update() {
       clearScreen();
       rectangle(0,0, totalWidth, totalHeight, "white");
     
     game.update();
    if(mouse.left) {
      let mark = game_board.giveBox(mouse);
      mark.symbole="x";
      game_board.addMark(mark);
    }
    };
