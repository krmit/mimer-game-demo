// By: krm 
// http://koda.nu/labbet/53382837 

   function Game() {
	   this.speed=5;
       this.points=0;
       this.crashes =[];
       const game_objects=[];
	   
	   this.add = function(gameObject) {
		   game_objects.push(gameObject);
	   }
       
       this.addCrash = function(gameObject) {
		   this.crashes.push(gameObject);
	   }
	   
	   this.update = function() {
		   for( let i = game_objects.length-1; i>=0; i--) {
              game_objects[i].update();
           }
	   }
   }
   
   const game = new Game();
  
  function Player() {
	  game.add(this);
      this.x = 10;
      this.y = 150;
    
      this.draw = function() {
         triangle(this.x, this.y,
                  this.x+10, this.y+10,
                  this.x,this.y+20, "red")
    };
    
      this.update = function() {
        if(getPixel(this.x+10,this.y+5).red===0 && 
           getPixel(this.x+10,this.y+5).green===0 && 
           getPixel(this.x+10,this.y+5).blue===0
         ) {
             game.addCrash(this);
        }

      this.draw();
       
      if(keyboard.left || keyboard.a) {
          this.x-=game.speed;
       }
   
      if(keyboard.right || keyboard.d) {
          this.x+=game.speed;
      }
    
      if(keyboard.up || keyboard.w) {
          this.y-=game.speed;
      }
    
      if(keyboard.down || keyboard.s) {
         this.y+=game.speed;
      }
	}
  };
  
  function Astro() {
	  game.add(this);
      this.x = 500;
      this.y = random(totalHeight);
      this.size =random(20)+5;
      this.speed =random(3)+1;
    
      this.draw = function() {
         circle(this.x, this.y,this.size, "black");
    };
      
      this.update = function() {
        this.draw();
        this.x = this.x - this.speed; 
      }
      
  };
  
   const player = new Player();
  
    function update() {
       game.points=game.points+0.1;
       let astro_chans=1-0.99/(1+(game.points/500));
       clearScreen();
       rectangle(0,0, totalWidth, totalHeight, "white");
      
      if(Math.random() < astro_chans) {
        new Astro();
      }
      
     game.update();
     text(10,50,40,Math.floor(game.points) + "/200","green");
     if(game.crashes.length > 0) { 
         rectangle(0,0, totalWidth, totalHeight, "red");
         text(50,200,60,"Game Over","blue");
         if(game.points < 200) {
             text(50,300,50,Math.floor(game.points),"yellow");
         }
         else {
             text(50,300,50,Math.floor(game.points),"green");
             text(150,300,50,"Good!","green");
         }
         stopUpdate();
     }
    };
