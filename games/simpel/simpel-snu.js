// By: krm 
// http://koda.nu/labbet/45667560

  const speed = 5;
  const player = {
    x: positionRandome(totalWidth),
    y: positionRandome(totalHeight)
  };
  
  const goal = {
    x: positionRandome(totalWidth),
    y: positionRandome(totalHeight)
  };
  
  let steps=0;
  const goal_distans=Math.floor(distance(player,goal)/3);
 
  function positionRandome(sideLength) {
   return random(sideLength-200)+100; 
  }
  
  function update() {
       clearScreen();
       circle(player.x, player.y,10, "blue");
       circle(goal.x,goal.y,10, "green");
       text(50,50, 40, steps, "blue");
       text(105,50, 40, "<", "red");
       text(135,50, 40, goal_distans, "green");
  
     if(keyboard.left || keyboard.a) {
        player.x-=speed;
        steps++;
     }
   
    if(keyboard.right || keyboard.d) {
        player.x+=speed;
       steps++;
     }
    
    if(keyboard.up || keyboard.w) {
        player.y-=speed;
      steps++;
     }
    
    if(keyboard.down || keyboard.s) {
        player.y+=speed;
      steps++;
     }
    
     if(getPixel(player.x,player.y).green === 128) {
       let msg = "Du är i mål! Det tog:"+steps+"\n";
       if(steps < goal_distans) {
           msg+="Bra! Du har vunnit!";
       }
       else {
          msg+="Skräp! Du har förlorat!";
       }
         
         
       alert(msg);
       stopUpdate();
     };
       
  }
